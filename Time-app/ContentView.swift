//
//  ContentView.swift
//  Time-app
//
//  Created by Joao Fiks on 09/02/21.
//

import SwiftUI

struct ContentView: View {
    @StateObject var viewModel = HomeViewModel()
    var body: some View {
        Text(viewModel.time)
            .padding()
            .font(.title)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
