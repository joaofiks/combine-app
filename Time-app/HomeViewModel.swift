//
//  HomeViewModel.swift
//  Time-app
//
//  Created by Joao Fiks on 09/02/21.
//

import Foundation
import Combine

final class HomeViewModel: ObservableObject {
    @Published var time = ""
    private var anyCancellable: AnyCancellable?
    
    let formatter: DateFormatter = {
        let df = DateFormatter()
        df.timeStyle = .medium
        return df
    }()
    
    init() {
        setupPublishers()
    }
    
    private func setupPublishers() {
        anyCancellable = Timer.publish(every: 1, on: .main, in: .default)
            .autoconnect()
            .receive(on: RunLoop.main)
            .sink { value in
                self.time = self.formatter.string(from: value)
            }
    }
}
