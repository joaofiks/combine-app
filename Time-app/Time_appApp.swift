//
//  Time_appApp.swift
//  Time-app
//
//  Created by Joao Fiks on 09/02/21.
//

import SwiftUI

@main
struct Time_appApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
